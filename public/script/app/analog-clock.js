var setClockTo = function (clockId, hours, minutes) {
  startClock(clockId, {
    startTime: (hours * 60 + minutes) * 60,
    noFace: true,
    tick: false,
    tz: 0,
    sLength: 0,
    sOverlap: 0,
    sThickness: 0,
    sBlob: 0,
    mBlob: 1.75
  });
  stopClock(clockId);
};

window.onload = function () {
  $(".clock").each(function () {
    var time = this.dataset["time"].split(":");
    setClockTo(this.id, parseInt(time[0], 10), parseInt(time[1], 10));
  });
};
