require File.expand_path('../app/tomin_app.rb', __FILE__)
use Rack::MethodOverride
use Rack::Static, urls: %w(/script /images /style), root: 'public'
run TominApp.new
