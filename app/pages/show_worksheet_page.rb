Dir[File.dirname(__FILE__) + '/exercise_presenters/*.rb'].each { |file| require file }

class ShowWorksheetPage
  class ExercisePresenter
    attr_reader :index

    def initialize(index, exercise)
      @exercise = exercise
      @index = index
      @presenter = Kernel.const_get("#{exercise.class}Presenter").new(exercise)
    end

    def problem
      presenter.problem
    end

    def text_field_name
      "solutions[#{index}]"
    end

    def form_group_classes
      if exercise.solved?
        if exercise.correct?
          "form-group has-feedback has-success"
        else
          "form-group has-feedback has-error"
        end
      else
        "form-group"
      end
    end

    def text_field_addon_classes
      if exercise.solved?
        if exercise.correct?
          "glyphicon glyphicon-ok form-control-feedback"
        else
          "glyphicon glyphicon-remove form-control-feedback"
        end
      end
    end

    def surrounding_css_class
      presenter.surrounding_css_class
    end

    def label_css_class
      presenter.label_css_class
    end

    def solution_control_css_class
      presenter.solution_control_css_class
    end

    def template
      presenter.template
    end

    def solution
      exercise.solution
    end

    private
    attr_reader :exercise, :presenter
  end

  def initialize(worksheet)
    @worksheet = worksheet
  end

  def put_worksheet_url
    "/worksheets/#{worksheet.id}"
  end

  def title
    "Werkblaadje #{worksheet.id}"
  end

  def active(link)
    "active" if link == :worksheets
  end

  def exercises
    worksheet.exercises.each_with_index.map { |exercise, index| ExercisePresenter.new(index, exercise) }
  end

  def css_files
    worksheet_presenter.css_files
  end


  def js_files
    worksheet_presenter.js_files
  end

  private
  attr_reader :worksheet

  def worksheet_presenter
    Kernel.const_get("ShowWorksheetPage::#{worksheet.type.to_s.split('_').collect(&:capitalize).join}WorksheetPresenter").new
  rescue NameError
    DefaultWorksheetPresenter.new
  end

  class TimeCountingWorksheetPresenter
    def css_files
      ["/style/analog-clock.css"]
    end

    def js_files
      ["/script/lib/march.js", "/script/app/analog-clock.js"]
    end
  end

  class AnalogToDigitalWorksheetPresenter
    def css_files
      ["/style/analog-clock.css"]
    end

    def js_files
      ["/script/lib/march.js", "/script/app/analog-clock.js"]
    end
  end

  class DefaultWorksheetPresenter
    def css_files
      []
    end

    def js_files
      []
    end
  end
end
