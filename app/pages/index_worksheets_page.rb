class IndexWorksheetsPage
  class WorksheetPresenter
    def initialize(worksheet)
      @worksheet = worksheet
    end

    def link
      "/worksheets/#{worksheet.id}"
    end

    def link_title
      "Werkblaadje #{worksheet.id}"
    end

    def score
      "#{worksheet.exercises.count(&:correct?)}/#{worksheet.exercises.count}"
    end

    private
    attr_reader :worksheet
  end

  def initialize(worksheets)
    @worksheets = worksheets
  end

  def worksheets
    @worksheets.map { |worksheet| WorksheetPresenter.new(worksheet) }
  end

  def title
    'Jouw werkblaadjes'
  end

  def active(link)
    "active" if link == :worksheets
  end

  def css_files
    []
  end

  def js_files
    []
  end
end
