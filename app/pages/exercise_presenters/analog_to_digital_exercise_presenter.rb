require_relative 'exercise_presenter'

class AnalogToDigitalExercisePresenter
  include ExercisePresenter

  def problem
    exercise.analog
  end

  def template
    :'exercises/analog_to_digital'
  end
end
