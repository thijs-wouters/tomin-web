module ExercisePresenter
  def initialize(exercise)
    @exercise = exercise
  end

  def template
    :'exercises/exercise'
  end

  private
  attr_reader :exercise
end
