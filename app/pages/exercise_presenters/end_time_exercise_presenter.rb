require_relative 'exercise_presenter'

class EndTimeExercisePresenter
  include ExercisePresenter

  def problem
    "De bakker begint met bakken om #{exercise.start}. Zijn brood moet #{exercise.duration} minuten in de oven staan. Hoe laat is het brood klaar?"
  end

  def template
    :"exercises/end_time"
  end
end
