require_relative 'exercise_presenter'

class MultiplicationExercisePresenter
  include ExercisePresenter

  def problem
    "#{exercise.left_hand_side} x #{exercise.right_hand_side} = "
  end
end
