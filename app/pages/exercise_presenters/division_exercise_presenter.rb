require_relative 'exercise_presenter'

class DivisionExercisePresenter
  include ExercisePresenter

  def problem
    "#{exercise.left_hand_side} : #{exercise.right_hand_side} = "
  end
end
