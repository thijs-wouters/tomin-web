require 'bundler/setup'

require 'sinatra/base'
require 'tomin'
require_relative 'pages/show_worksheet_page'
require_relative 'pages/index_worksheets_page'

require 'yaml/store'
Chassis.repo.register :pstore, Chassis::PStoreRepo.new(YAML::Store.new('data.yml'))
Chassis.repo.use :pstore

class TominApp < Sinatra::Base
  [
    '/',
    '/worksheets',
  ].each do |url|
    get url do
      @view = IndexWorksheetsPage.new(WorksheetRepo.all)
      erb :'worksheets/index'
    end
  end

  post '/worksheets' do
    form = RequestWorksheetForm.new(type: params[:type])
    new_worksheet = RequestWorksheet.new(form, nil).execute
    redirect "/worksheets/#{new_worksheet.id}"
  end

  get '/worksheets/:id' do
    @view = ShowWorksheetPage.new(WorksheetRepo.find(params[:id].to_i))
    erb :'worksheets/show'
  end

  put '/worksheets/:id' do
    form = SubmitWorksheetForm.new do |form|
      form.solutions = params['solutions'].values
    end
    SubmitWorksheet.new(form).execute(params[:id].to_i)
    redirect "/worksheets/#{params[:id]}"
  end
end
