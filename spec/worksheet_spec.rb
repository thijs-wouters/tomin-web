require 'rspec'
  
RSpec.describe 'requesting a worksheet', type: :feature do
  it 'requests a new worksheet' do
    expect(WorksheetRepo).to be_empty
    visit '/'
    click_button 'Nieuw werkblaadje'
    expect(page).to have_content('Geef af')
    expect(WorksheetRepo).not_to be_empty
  end

  it 'submits a new worksheet' do
    worksheet = Worksheet.create do |w|
      w.exercises = [
        MultiplicationExercise.new(left_hand_side: 1, right_hand_side: 2),
        MultiplicationExercise.new,
        MultiplicationExercise.new,
        MultiplicationExercise.new,
      ]
    end

    visit "/worksheets/#{worksheet.id}"
    expect(page).to have_content('1 x 2 =')
    click_button 'Geef af'
    expect(page).to have_content('1 x 2 =')
  end
end
