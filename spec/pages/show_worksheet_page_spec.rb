RSpec.describe ShowWorksheetPage do
  describe ShowWorksheetPage::ExercisePresenter do
    [
      [
        DivisionExercise.new(left_hand_side: 3, right_hand_side: 3),
        '3 : 3 = ',
        :'exercises/exercise',
      ],
      [
        MultiplicationExercise.new(left_hand_side: 3, right_hand_side: 3),
        '3 x 3 = ',
        :'exercises/exercise',
      ],
      [
        EndTimeExercise.new(start: Tomin::Clock.new(hours: 1, minutes: 30), duration: 30),
        'De bakker begint met bakken om 1:30. Zijn brood moet 30 minuten in de oven staan. Hoe laat is het brood klaar?',
        :'exercises/end_time',
      ],
    ].each do |exercise, expected_problem, expected_template|
      context "with a #{exercise.class}" do
        subject { ShowWorksheetPage::ExercisePresenter.new(1, exercise) }

        describe '#problem' do
          it "is #{expected_problem}" do
            expect(subject.problem).to eq(expected_problem)
          end
        end

        describe '#template' do
          it "returns #{expected_template}" do
            expect(subject.template).to eq(expected_template)
          end
        end
      end
    end
  end

  [
    [:tables_of_multiplication, [], []],
    [:tables_of_division, [], []],
    [:time_counting, ['/script/lib/march.js', '/script/app/analog-clock.js'], ['/style/analog-clock.css']],
  ].each do |type, expected_js_files, expected_css_files|
    context "when the type of the worksheet is #{type}" do
      subject { ShowWorksheetPage.new(Worksheet.new(type: type)) }

      describe '#js_files' do
        it "returns #{expected_js_files}" do
          expect(subject.js_files).to eq(expected_js_files)
        end
      end

      describe '#css_files' do
        it "returns #{expected_css_files}" do
          expect(subject.css_files).to eq(expected_css_files)
        end
      end
    end
  end
end
